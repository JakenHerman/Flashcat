# [Flashcat](http://www.jaken.me/Flashcat)
Multi Application Installer and Updater

[![Build Status](https://travis-ci.org/JakenHerman/Flashcat.svg?branch=master)](https://travis-ci.org/JakenHerman/Flashcat)

**Introduction**

This application is intended to be a jump-starter for newer computer users or even for veterans to get a jump start on a new device. It will allow a user to select a group of common software to download and maintain without the user having to individually download and update every single one. 


**Simplest Usage**

Just run the `.jar` file. If you'd like to physically re-build and run the code from an Integrated Development Environment, use the `flashcat.java` file, as it is the driver for other files.
